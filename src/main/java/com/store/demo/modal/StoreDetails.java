package com.store.demo.modal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "source_details")
public class StoreDetails implements Serializable {

	private static final long serialVersionUID = 2492595427488688833L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@Column(name = "store_name", length = 100, nullable = false)
	private String storeName;

	@Column(name = "store_address", length = 100, nullable = false)
	private String storeAddress;

	@Column(name = "store_pincode", nullable = false)
	private Long storePincode;
	
	public StoreDetails() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreAddress() {
		return storeAddress;
	}

	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}

	public Long getStorePincode() {
		return storePincode;
	}

	public void setStorePincode(Long storePincode) {
		this.storePincode = storePincode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
