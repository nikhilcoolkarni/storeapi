package com.store.demo.service;

public interface StoreService {

	String add(String storeInfo);

	String welcome();

	String list();

	String deleteById(Long Id);

	String updateStore(String storeUpdateDetails);

	String search(Long startPin, Long endPin);

}
