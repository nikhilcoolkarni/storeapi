package com.store.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.google.gson.Gson;
import com.store.demo.modal.StoreDetails;
import com.store.demo.repository.IStoreRePository;
import com.store.demo.service.StoreService;

@EnableWebMvc
@RestController
@Service("storeService")
public class StoreServiceImpl implements StoreService {

	@Autowired
	private IStoreRePository iStoreRePository;
	
	@Override
	@ResponseBody
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String welcome() {
		return "welcome";
	}
	
	@Override
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String add(@RequestBody String storeStr) {
		try {
			StoreDetails storeDetails = new Gson().fromJson(storeStr,StoreDetails.class);
			if(storeDetails != null) {
				iStoreRePository.save(storeDetails);
				return "SUCCESS";
			}
		} catch (Exception e) {
			return e.getMessage();
		}
		return "FAIL";
	}
	
	@Override
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String list() {
		List<StoreDetails> storeDetails = iStoreRePository.findAll();
		return new Gson().toJson(storeDetails);
	}
	
	@Override
	@ResponseBody
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String deleteById(@PathVariable("id") Long Id) {
		try {
			iStoreRePository.delete(Id);
			return "SUCCESS";
			
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	@Override
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String updateStore(@RequestBody String storeUpdateDetails) {
		StoreDetails updateString = new Gson().fromJson(storeUpdateDetails,StoreDetails.class);
		iStoreRePository.updateStoreContent(updateString.getId(),updateString.getStoreName(),updateString.getStoreAddress(),updateString.getStorePincode());
		
		return "SUCCESS";
	}
	
	@Override
	@ResponseBody
	@RequestMapping(value = "/search/{startPin}/{endPin}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String search(@PathVariable("startPin") Long startPin ,@PathVariable("endPin") Long endPin) {
		List<StoreDetails> storeDetails = iStoreRePository.search(startPin,endPin);
		return new Gson().toJson(storeDetails);
	}
}