package com.store.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.store.demo.modal.StoreDetails;

public interface IStoreRePository extends JpaRepository<StoreDetails, Long> {
	
	
	@Modifying
	@Transactional
	@Query("UPDATE StoreDetails s SET s.storeName =:storeName, s.storeAddress =:storeAddress, s.storePincode =:storePincode WHERE s.id =:id")
	void updateStoreContent(@Param("id")Long id,@Param("storeName") String storeName,@Param("storeAddress") String storeAddress,@Param("storePincode")Long storePincode);

	@Query("SELECT s FROM StoreDetails s WHERE s.storePincode BETWEEN :startPin AND :endPin")
	List<StoreDetails> search(@Param("startPin")Long startPin,@Param("endPin") Long endPin);
	

}
